#!/bin/bash
#
# update_cache.sh : a script to download a dwca set to be used as a cached version
#
# Usage: ./update_cache.sh [data set name]
#
# E.g.:  ./update_cache.sh crustacea

# Constants and Variables

host="localhost:8080"
cache_dir="/data/dwca/cache"

if [ -z $1 ]; then
  echo "You need to provide a dwca set name. E.g.: ./update_cash.sh tunicata"
  exit 1
fi
dwca_set=$1

echo "Start updating cache for dwca set $dwca_set"


if [ "$dwca_set" = "nsr" ] || [ "$dwca_set" = "dcsr" ]; then
  type="taxon"
else
  type="specimen"
fi

cd $cache_dir/$type
curl $host/v2/$type/dwca/getDataSet/$dwca_set --output $dwca_set.dwca.zip.in_progress
if [ $? -gt 0 ]; then
  rm $dwca_set.dwca.zip.in_progress
  echo "ERROR: creation of dwca cache file failed!"
  exit 1
fi

file_info=$( file $dwca_set.dwca.zip.in_progress )
grep "Zip archive data" <<< $file_info
if [ $? -gt 0 ]; then
  mv $dwca_set.dwca.zip.in_progress $dwca_set.dwca.zip.failed
  echo "ERROR: creation of dwca cache file failed. Could not download a valid zip file"
  exit 1
fi

mv $dwca_set.dwca.zip.in_progress $dwca_set.dwca.zip
echo "Cache for dwca set $dwca_set has been updated"
