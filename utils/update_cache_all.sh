#!/bin/bash
#
# update_cache_all.sh : a shell script for updating the nba dwca cache

# Constants and Variables

host="localhost:8080"
cache_dir="/data/dwca/cache"
types=(specimen taxon)
failed=()
downloads=0

# Functions

is_root() {
  if [[ $(whoami) != "root" ]]; then
    echo "ERROR: Please run script as root user!"
    exit 1
  fi
}

download_dwca() {
  set_name=$1
  echo "Started download of dwca set $set_name"
  error=$(curl $host/v2/$type/dwca/getDataSet/$set_name --output $set_name.dwca.zip.in_progress)
  if [[ $? -gt 0 ]]; then
    echo "ERROR: downloading dwca set $set_name failed!"
    echo "ERROR: $error"
    failed+=" $set_name"
  else
    mv $set_name.dwca.zip.in_progress $set_name.dwca.zip
    echo "INFO: dwca set $set_name has been download: $set_name.dwca.zip"
    downloads=$( expr $downloads + 1 )
  fi
}


# Main

is_root

for type in ${types[@]}
do
  cd $cache_dir/$type
  if [[ $? > 0 ]]; then
    echo "ERROR: dwca directory is missing: $cache_dir/$type"
    echo "ERROR: downloading dwca sets failed."
    exit 1
  fi

  # retrieve the data set names for the specified type
  echo "INFO: retrieving dwca data set names for document type: $type."
  data_sets_str=$(curl -f $host/v2/$type/dwca/getDataSetNames)
  if [[ $? > 0 ]]; then
    echo "ERROR: Could not retrieve list of data set names: $data_sets_str"
    echo "Check url: $host/v2/$type/dwca/getDataSetNames"
    exit $?
  fi

  # replace a , with a white space, and remove the quotes and square brackets 
  data_sets_str=$(echo $data_sets_str | tr ',' ' ')
  data_sets_str=$(echo $data_sets_str | tr -d '[]')
  data_sets_str=$(echo $data_sets_str | tr -d '"')

  # create an array
  data_sets=($data_sets_str);

  # download the dwca sets for the specified type
  for set_name in ${data_sets[@]}; do
    download_dwca $set_name
  done

done


failed=($failed)
if [[ ${#failed[@]} > 0 ]]; then
  echo "ERROR: ${#failed[@]} dwca sets failed to download!"
  exit 1
else
  echo "INFO: $downloads dwca sets have been downloaded."
  exit 0
fi
