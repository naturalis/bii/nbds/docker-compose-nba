#!/bin/bash

curl -s http://localhost:9200 > /dev/null

if [ $? -ne 0 ]; then
  echo "Server is down"
  exit 1
fi

if [ "$1" = "--reset" ]
then
  echo $(curl -s -XPUT 'http://localhost:9200/_cluster/settings' -H "Content-Type: application/json" -d '{ "transient" : { "indices.breaker.request.limit": null, "indices.breaker.total.limit" : null } }')
elif [ "$1" = "--info" ]
then
  echo $(curl -s -XGET 'http://localhost:9200/_cluster/settings')
else
  echo $(curl -s -XPUT 'http://localhost:9200/_cluster/settings' -H "Content-Type: application/json" -d '{ "transient" : { "indices.breaker.request.limit" : "65%", "indices.breaker.total.limit" : "80%" }}')
fi


